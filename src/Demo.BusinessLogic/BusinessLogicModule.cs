﻿using Demo.BusinessLogic.Contracts;
using Demo.BusinessLogic.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.BusinessLogic
{
    public static class BusinessLogicModule
    {
        public static IServiceCollection AddBusinessLogicModule(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();
            return services;
        }
    }
}
