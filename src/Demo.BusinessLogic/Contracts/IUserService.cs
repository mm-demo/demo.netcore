﻿using Demo.Common.Contracts;
using System.Threading.Tasks;

namespace Demo.BusinessLogic.Contracts
{
    public interface IUserService
    {
        Task CreateUser(User user);
    }
}
