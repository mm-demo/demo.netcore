﻿using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApi
{
    public class DemoRoutePrefix : RouteAttribute
    {
        public DemoRoutePrefix(string apiVersionPrefix, string routePrefix)
            : base(apiVersionPrefix + routePrefix)
        { }
    }
}
