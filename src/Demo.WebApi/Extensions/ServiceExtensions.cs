﻿using AutoMapper;
using Demo.WebApi.Requests;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddMappingProfiles(this IServiceCollection services)
        {
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<RequestMappingProfile>();
            }).CreateMapper());
        }
    }
}
