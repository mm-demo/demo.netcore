﻿using AutoMapper;
using Demo.Common.Contracts;

namespace Demo.WebApi.Requests
{
    public class RequestMappingProfile : Profile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Reviewed. This is ok as this is where we create all the request mapping.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Reviewed. This is ok as this is where we create all the request mapping.")]
        public RequestMappingProfile()
        {
            CreateMap<UserCreateRequest, User>();
        }
    }
}
