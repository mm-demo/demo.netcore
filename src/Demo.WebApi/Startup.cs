﻿using Demo.BusinessLogic;
using Demo.WebApi.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO;

namespace Demo.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddControllersAsServices();

            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1_0", new OpenApiInfo { Title = "Demo Web Api", Version = "1.0" });
                option.SwaggerDoc("v1_1", new OpenApiInfo { Title = "Demo Web Api", Version = "1.1" });
                
                //Tell Swagger to use XML comments added in controller
                option.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, typeof(Startup).Namespace + ".xml"));
            });

            services.AddMappingProfiles();
            services.AddBusinessLogicModule();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint($"v1_0/swagger.json", "1.0");
                option.SwaggerEndpoint($"v1_1/swagger.json", "1.1");
                option.DocExpansion(DocExpansion.List);
            });
            app.UseMvc();
        }
    }
}
