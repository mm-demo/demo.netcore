﻿using AutoMapper;
using Demo.BusinessLogic.Contracts;
using Demo.Common.Contracts;
using Demo.WebApi.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Net;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Annotations;

namespace Demo.WebApi.Controllers
{
    [DemoRoutePrefix(Constants.RoutePrefix10_11, "users")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns>The list of all available users</returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "The requested resource could not be found.")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(User))]
        public async Task<IActionResult> GetUsers()
        {
            await Task.CompletedTask;
            return Ok(new User() { FirstName = "Test", LastName = "Test" });
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="user">The user request</param>
        [HttpPost]
        [Route("")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "INVALID_PARAMETER: One or more query parameters are invalid.")]
        [SwaggerResponse((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateUser([FromBody, BindRequired] UserCreateRequest user)
        {
            await _userService.CreateUser(_mapper.Map<User>(user));
            return StatusCode((int)HttpStatusCode.Created);
        }
    }
}