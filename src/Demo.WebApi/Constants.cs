﻿namespace Demo.WebApi
{
    public class Constants
    {
        public const string RoutePrefix10_11 = "api/{version:regex(1.0|1.1)}/";
        public const string RoutePrefix10 = "api/{version:regex(1.0)}/";
    }
}
